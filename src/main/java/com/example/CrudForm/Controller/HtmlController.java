package com.example.CrudForm.Controller;


import com.example.CrudForm.Module.Info;
import com.example.CrudForm.Repository.DataRepository;
import com.example.CrudForm.Service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.Optional;

@Controller
@RequestMapping("/html")
public class HtmlController {
    @Autowired
    DataService dataService;
    @Autowired
    DataRepository dataRepository;


        @RequestMapping(value = "/tas", method = RequestMethod.GET)
        public ModelAndView taskList() {

            ModelAndView modelAndView=new ModelAndView();

            modelAndView.addObject("tasks",dataService.findAll());
            modelAndView.setViewName("getall");
            return modelAndView;
        }


    @RequestMapping("/user")
    public String Form() {

        return "Form";
    }
    @RequestMapping(value="/adddata")

    public String adddata(@RequestParam("name") String name,@RequestParam("rollno") int rollno,@RequestParam("phnno") String phnno,@RequestParam("state") String state) {
        Info details=new Info();


        details.setname(name);
        details.setrollno(rollno);
        details.setphnno(phnno);
        details.setstate(state);

            Info b=this.dataService.adddata(details);
        System.out.print(b);
        return "success";
    }
    @PostMapping(value="/editcont/{id}")
    public String updateIt(@PathVariable(required=false) long id,@RequestParam(required=false) String name,@RequestParam(required=false) int rollno,@RequestParam(required=false) String phnno,@RequestParam(required=false) String state) {
        Info details=new Info();
        System.out.println(name);
        System.out.println(id);
        if(id!=0)
            details.setId(id);

        if(name!=null)
        details.setname(name);

        if(rollno!=0)
        details.setrollno(rollno);

        if(phnno!=null)
        details.setphnno(phnno);

        if(state!=null)
        details.setstate(state);

            Info b=dataService.updateRecord(details,id);
            System.out.println(b);
        return "redirect:/html/tas";
    }
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable long id) {
        dataService.deleteById(id);
        return "redirect:/html/tas";
    }
    @RequestMapping(value="/formredirect/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Optional<Info> inf=dataService.findById(id);
        modelAndView.addObject("val",inf.get());
        modelAndView.setViewName("editform");
        return modelAndView;
    }
    }
