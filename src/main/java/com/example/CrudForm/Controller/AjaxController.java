package com.example.CrudForm.Controller;

import com.example.CrudForm.Module.Info;
import com.example.CrudForm.Service.DataService;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/ajax")
public class AjaxController {
    @Autowired
    DataService dataService;

    @GetMapping("/get")
    public String indexform()
    {

        return "index";
    }
    @PostMapping("/saveUser")
    public @ResponseBody String saveUser(@RequestParam("name") String name
                                  ,@RequestParam("rollno") int rollno,@RequestParam("phnno") String phnno,
                                         @RequestParam("state") String state,@RequestParam("image") String image) throws Exception
    {
        Info inf=dataService.saveUser(name,rollno,phnno,state,image);
        JSONObject obj=new JSONObject();


        obj.put("name",inf.getname());
        obj.put("rollno",inf.getrollno());
        obj.put("phnno",inf.getphnno());
        obj.put("state",inf.getstate());
        obj.put("image",inf.getimage());

        return  obj.toString();

    }@GetMapping("/getajax")
    public String index()
    {

        return "getallajax";
    }

    @RequestMapping(value="/getall", method = RequestMethod.GET)
    public @ResponseBody List<Info> check() {

    List<Info> inf=dataService.findAll();
        return inf;
    }

    @GetMapping("/editajax")
    public String edit()
    {
        return "formajax";
    }

    @RequestMapping(value = "/del/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id) {
        System.out.println(id);

        dataService.deleteById(id);
        return "redirect:/ajax/getajax";
    }
    @RequestMapping(value="/redirectajax/{id}")
    public ModelAndView redirect(@PathVariable long id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Optional<Info> inf=dataService.findById(id);
        modelAndView.addObject("val",inf.get());
        modelAndView.setViewName("formajax");
        return modelAndView;
    }
    @RequestMapping(value="/editUser",method=RequestMethod.POST)
    public @ResponseBody Info updateIt(@RequestParam(required=true) long id,
                           @RequestParam(required=false) String name,@RequestParam(required=false) int rollno,
                           @RequestParam(required=false) String phnno,@RequestParam(required=false) String state,
                                       @RequestParam(required=false) String image) throws Exception {
        Info details=new Info();
        System.out.println(name);
        System.out.println(id);
        if(id!=0)
            details.setId(id);

        if(name!=null)
            details.setname(name);

        if(rollno!=0)
            details.setrollno(rollno);

        if(phnno.length()<10 || phnno.length()>10)
        {
            throw new Exception("Invalid phnno");
        }
        else
        {
            details.setphnno(phnno);
        }

        if(state!=null)
            details.setstate(state);
        if(image!=null)
            details.setimage(image);

        Info b=dataService.updateRecord(details,id);
        System.out.println(b);
        return b;
    }



}
