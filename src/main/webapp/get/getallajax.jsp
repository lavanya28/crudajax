<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

    <head>
     <meta charset="utf-8">
                 <meta name="viewport" content="height=device-height, width=device-width, initial-scale=1.0,
                  minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, target-densitydpi=device-dpi">



                 <link
                         rel="stylesheet"
                         href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
                         integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
                         crossorigin="anonymous"
                 />

                 <link
                         rel="stylesheezt"
                         href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
                         integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
                         crossorigin="anonymous"
                 />

                <style>

          #topnav {

            background-color: white;
            color:#7A9DF8;

          }
          #topnav a{
            font-size: 17px;
            color:black;
          }
          .navbar-nav.navbar-center {
             position: absolute;
             left: 50%;
             transform: translatex(-50%);
          }
          body{font-family: Arial, Helvetica, sans-serif;
          }
                </style>
     <script src="/js/user.js"></script>
     <script src="https://use.fontawesome.com/c970c9d5cb.js"></script>
     </head>
    <body  style="background-color: #F1EFF5;">
    <header class="header bg-black m-0 sticky-top">
  <nav class="navbar navbar-expand-lg fixed-top" id="topnav">
       <a class="nav navbar-nav navbar-left" href=""> <img src="/image/bizrise.png" style="height:50; width:50;/></a>
           <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
           </button>

       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="nav navbar-nav navbar-center">
             <li class="nav-item active">
                 <a class="nav-link col-xs-5" style="color:#7A9DF8;" href="/ajax/getajax">HOME <span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item active">
                <a class="nav-link col-xs-5" style="color:#7A9DF8;" href=""><span class="sr-only">(current)</span></a>
             </li>
             <li class="nav-item active">
                 <a class="nav-link col-xs-3" style="color:#7A9DF8;" href="/ajax/get">AddUser <span class="sr-only">(current)</span></a>
             </li>

          </ul>
  </nav>
  </header>
  <br><br><br><br><br>
      <div id="show" class="data"></div>

<script
                src="https://code.jquery.com/jquery-3.6.0.min.js"
                integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
                crossorigin="anonymous"
        ></script>
        <script
                src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
                integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
                crossorigin="anonymous"
        ></script>
        <script
                src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"
                integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf"
                crossorigin="anonymous"
        ></script>
        <script>
              $('document').ready(function() {
                    getAllUsers();
               });
        </script>
        <script src="../js/user.js"></script>



</body>
</html>